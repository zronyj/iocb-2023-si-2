# NAMD Config file - autogenerated by Rony's Script
# Author: Rony Letona,  zronyj@gmail.com

# Input
amber                   yes
ambercoor               /home/zronyj/Git/iocb-2023-si/MD/tartaric_wb.inpcrd
parmfile                /home/zronyj/Git/iocb-2023-si/MD/tartaric_wb.prmtop

# Temperature control
set temperature         298
temperature             $temperature;  # initial temperature

# Force-Field Parameters
exclude                 scaled1-4
1-4scaling              1.0

# Simulation space partitioning
cutoff                  12.0
switching               on
switchdist              9.0
pairlistdist            14.0

# Integrator Parameters
timestep                1.0
rigidBonds              none
nonbondedFreq           1
fullElectFrequency      1  
stepspercycle           1000

# Constant Temperature Control
langevin                on
langevinDamping         1      ;# damping coefficient (gamma) of 1/ps
langevinTemp            $temperature
langevinHydrogen        off    ;# don't couple langevin bath to hydrogens

# Periodic Boundary Conditions
cellBasisVector1 40.0310001373291 0 0
cellBasisVector2 0 40.04500198364258 0
cellBasisVector3 0 0 40.08799934387207
cellOrigin 40.065826416015625 39.78861618041992 39.96796798706055
wrapAll                 on

# PME (for full-system periodic electrostatics)
PME                     on
PMEGridSpacing          1.0

# Constant Pressure Control (variable volume)
useGroupPressure        no
useFlexibleCell         no
useConstantArea         no

langevinPiston          on
langevinPistonTarget    1.01325 ;#  in bar -> 1 atm
langevinPistonPeriod    500.0
langevinPistonDecay     50.0
langevinPistonTemp      $temperature

# Output
set output              /home/zronyj/Git/iocb-2023-si/MD/tartaric_wb/tartaric_wb
outputname              $output
dcdfile                 ${output}.dcd
xstFile                 ${output}.xst
dcdfreq                 500
xstFreq                 500

binaryoutput            no
binaryrestart           no
outputEnergies          500
restartfreq             1000000

fixedAtoms              off

# Basic dynamics
COMmotion               no
dielectric              1.0

# Scripting

minimize                10000
reinitvels              $temperature
run                     10000000