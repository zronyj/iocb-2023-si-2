mol load pdb *.pdb dcd *.dcd
set nf [molinfo top get numframes]
for {set i 0 } {$i < $nf} {incr i} {
 [atomselect top all frame $i] writepdb frame_$i.pdb
}
exit