# IOCB 2023 Summer Internship

## Purpose and Objective

This repository is the continuation of the [previous repository](https://gitlab.com/zronyj/iocb-2023-si), where I kept track all the files, scripts and programs I used during the summer. The idea of the project was to find out **the effect of water molecules on a solute in Raman Optical Activity spectroscopy**.

## Structure

The files in the repository were organized according to a workflow:

1. The **Molecules** directory contains the files for the water and tartaric acid molecules. They were optimized ([ORCA](https://sites.google.com/site/orcainputlibrary/home)), and their RESP charges were computed ([Multiwfn](http://sobereva.com/multiwfn/)). Afterwards, the tartaric acid was solvated with water molecules into 3 different box sizes, using the [PACKMOL](https://m3g.github.io/packmol/) package. The directories are listed as follows:
    - water
    - tartaric
    - 20A
    - 30A
    - 40A
2. The **MD** directory contains the results (without the trajectory!) of the Molecular Dynamics simulations for all 3 box sizes. The MD simulations were carried out using [NAMD](https://www.ks.uiuc.edu/Research/namd/) with the Amber Force Field. Subsequently, the trajectory was analyzed with MDAnalysis (tartaric acid molecule centered and wrapped), and snapshots were taken from it. All of this was done with [VMD](https://www.ks.uiuc.edu/Research/vmd/) at the beginning, but a Python script was written to make it more straightforward.
    - 20A
    - 30A
    - 40A
3. The **QGRAD** directory contains snapshots of the MD simulation treated with another Python program to dice the system into smaller boxes. Also, some scripts are included in there to prepare all input files for future simulations.
    - frame_1500
    - frame_17500
    - combined
    - remap
    - again
4. The **moleditoolbox** directory contains many small programs, but the most important file is *moledito.py* which is the library made to handle the splitting of the system into smaller boxes. It is also capable to analyze bond lengths, angles, dihedrals, etc. But most importantly, the `Cluster` class in it is the one capable of handling systems of molecules (clusters) and analyzing them.

## Usage
Unfortunately, 2 months were a short time to understand the project and carry it out considering some errors and issues along the way. Therefore, the most important information in this directory are the programs. The rest of the data is provided as a reference, since some of the calculations have to be done again.

### Installation
The scripts for this project were written mainly in Python, but also in TCL and Bash. The latter is available out of the box in almost all Linux systems. TCL was used for some tasks in VMD but was then replaced by Python scripts. And for Python, I provide some instructions on how to proceed.

It should be noted that the **Python** version to run these programs should be **3.9 or higher**! Otherwise, many errors will arise.

#### PIP - Python Package Index
If you choose to install Python from scratch, an option is to download it from its [main website](https://www.python.org/). There are several options to do this on Linux (from source) or on Windows; the programs should run fine on both OSs. After installing it, you will have access to the `pip` command. Therefore, the following packages, the installation instructions, and their documentation sites are listed here:

- NumPy: `pip install numpy` - [docs](https://numpy.org/doc/stable/)
- MatPlotLib: `pip install matplotlib` - [docs](https://matplotlib.org/stable/api/index)
- MDAnalysis: `pip install --upgrade MDAnalysis` - [docs](https://docs.mdanalysis.org/stable/index.html)

#### Conda - Anaconda
If you choose to install a whole Python working environment (recommended if you will be doing something else), then the [Anaconda](https://www.anaconda.com/) suite is a better option. Currently, it offers the interface to MS Excel, so for Windows, it's a nice choice. It should be noted that Anaconda is way bigger than a basic Python installation. Once you have it installed, then the `conda` command will be available for you. It is a good practice to create a local environment to work on different projects, so the steps to do so are also provided here. The pagkages you need can be installed as follows:

- Environment:
    - Create it: `conda create -n my-environment`
    - Activate it: `conda activate my-environment`
    - Add software channels: `conda config --env --add channels conda-forge`
    - Deactivate it (use this one after you're done working): `conda deactivate`
- Packages:
    - NumPy: `conda install numpy` - [docs](https://numpy.org/doc/stable/)
    - MatPlotLib: `conda install matplotlib` - [docs](https://matplotlib.org/stable/api/index)
    - MDAnalysis: `conda install MDAnalysis` - [docs](https://docs.mdanalysis.org/stable/index.html)

### Programs
The main idea for the summer was to produce some pieces of software which helped in the calculations. To do the latter, a library was created which provided the user with the capabilities to handle molecules and solvated clusters. Additionally, MDAnalysis was used, but extending this library would have been a little trickier than just extending a very basic one. Therefore, *moleditoolbox* and *MDAnalysis* became complementary in this work. Several scripts/programs were developed based on these libraries which will be described now, in order of the project.

> Note: Most programs have a help feature. This means that a description of the program and its arguments is provided if you run the program with the `--help` flag in the command line.

1. `md_postprocessing.py` - it will read the structure and trajectory of an MD simulation, center and wrap the molecule of interest, and extract snapshots of it during the trajectory. It has a help feature.
2. `dihedrogram.py` - it will analyze a trajectory for a specific dihedral angle, and it will plot it. It has a help feature.
3. `solvshells.py` - it will analyze the number of molecules at a certain distance of a solute in an MD trajectory, and plot the abundance and average. It has a help feature.
4. `splitboxer.py` - it splits the solvation box into smaller boxes or sub-clusters (with or without overlaps), and it saves the files in XYZ format, with its respective map of the sub-cluster relative to the big cluster/solvation box. It has a help feature.
5. `xinp.py` - it will read all the XYZ files and produce directories for each one with a Gaussian input file for frequencies (e.g. `cluster_XXX.inp`), and `FILE.X`, `G98.INP` and `FTRY.INP` files. It has a help feature.
6. `qsub_maker.py` - it will read a `template.sh` file with the general layout of a PBS job launcher file, and it will create launching files for all directories in the current working directory.
7. `pre_cctn.py` - it reads all the maps, checks that are correct, and produces a `CCT.INP` file for subsequent execution of the `cctn` program.

### Scripts
Mostly located in the **QGRAD** folder, they were attempts at automating several tasks. The most important ones are listed here.

1. `template.sh` - it will become the PBS launcher script for every folder. So all the calculation parameters should be set here. This script is to be copied into the directory above the cluster directories.
2. `pre_combine.sh` - it will run `gar9` on the output files after running all the Gaussian jobs, and will move the files into separate folders. It should be noted that the output files will be renamed!
3. `get_spectra.sh` - it will run `gar9`, `new1`, `new2`, `new4`, `new5`, `new6` and `tabprnf` on each files of the cluster folders. Please keep in mind that `new4` will interactively ask for input data, since the frequencies of each calculation are shown until that exact moment.

## Issues and Shortcomings
I made several mistakes and had several issues when creating some of these programs and doing some of the simulations. The most important ones I leave to you in case you wish to make a better job than I did.

1. The MD with an Amber FF lead to a strange pyramidal geometry of the carboxyl group in the molecule. Since the parametrization was done correctly (AFAIK), I am left wondering why did this happen.
2. When transferring the files from a Windows computer to the Linux cluster, the whitespaces and new line characters were changed, because the files aquired the DOS format. To correct some of these problems, the `rephrase.py` script is included in the **moleditools** folder. Please place it in every cluster folder in order to avoid this.
3. `moledito.py` was originally building the boxes using the `set` object in Python. The problem is that `set` is not an ordered array, so time that the map for the cluster was built, the map came out differently. I corrected this in the file, but the calculations had been made. Therefore, I am still looking for a way to re-create the maps and join everything to be analyzed by `cctn`.