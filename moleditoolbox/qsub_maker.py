import os

with open('template.sh', 'r') as t:
    template = t.read()

here = os.getcwd()
dirs = [d for d in os.listdir(here) if os.path.isdir(os.path.join(here, d))]

for d in dirs:
    os.chdir(os.path.join(here, d))
    with open('qsub_send.sh', 'w') as f:
        f.write(template.format(jname=d))
    os.chdir(here)
    print(f"{d} is finished!")