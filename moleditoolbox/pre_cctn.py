import os

def matcher(megacluster, subcluster, substructure_map):

    with open(megacluster, 'r') as mc:
        big_coords = mc.readlines()
    
    nbatoms = int(big_coords[1].split()[0])
    big_coords = big_coords[2:]
    big_coords = [ a.split() for a in big_coords ]

    with open(subcluster, 'r') as sc:
        little_coords = sc.readlines()
    
    little_coords = little_coords[2:]
    little_coords = [ a.split() for a in little_coords ]

    with open(substructure_map, 'r') as sm:
        little_map = sm.readlines()
    
    little_map = [ [int(l[i:i+3]) for i in range(0, len(l), 3) if l[i:i+3] != "\n"] for l in little_map ]
    mol_pointer = int(len(little_map)/2)

    map1 = sum(little_map[:mol_pointer], [])
    map2 = sum(little_map[mol_pointer:], [])

    # Sanity check
    if len(map1) != len(map2):
        raise ValueError(f"Missmatch between the maps in {substructure_map}!")
    if len(map1) != nbatoms:
        raise ValueError(f"The number of atoms in {megacluster} is not the same as {substructure_map}!")

    for i in range(nbatoms):
        bp = int(map1[i]) - 1
        lp = int(map2[i]) - 1
        if lp != -1:
            atom_i = int(big_coords[bp][0])
            atom_j = int(little_coords[lp][0])
            if atom_i != atom_j:
                raise ValueError(f"Atom missmatch at position {i} for {subcluster}: {atom_i} vs {atom_j}")

# Get the current working directory
here = os.getcwd()

# Prepare the template
comb_template = """IWG
0
OKI
0
LOFF
t
LDIAG
t
LABS
t
LVCD
t
LRAM
t
LROA
t
LHALTONERROR
f
LNAMES
t
POLYMER
        {boxes}
"""

# Get the names of the clusters
clusters = os.listdir(here)
clusters = [c[:-2] for c in clusters if c[-2:] == ".x"]

# Get the number of clusters
n_clusters = len(clusters)

# Prepare the output
output = comb_template.format(boxes=n_clusters)

# Iterate over the names of the clusters
for i, c in enumerate(clusters):
    
    # Add number of cluster data to the output
    output += f"        {i+1}\n"

    # Add the name of the files CCTN should look for
    output += c + "\n"

    # Extract the map indices to read the map data
    j, k, l = c[8:11]

    matcher("BIG.X", f"{c}.x", f"map_{j}-{k}-{l}.dat")

    # Get the whole map
    with open(f"map_{j}-{k}-{l}.dat", "r") as m:
        data = m.read()
    
    # Add the data to the output
    output += data + "\n"

with open("CCT.INP", "w") as final:
    final.write(output)