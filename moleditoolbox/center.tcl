# Load the system
mol load pdb tartaric_wb.pdb dcd tartaric_wb.dcd
# Set the Periodic Boundary Conditions box size
pbc set {40 40 40} -all
# Center the molecule of interest
pbc wrap -centersel "not waters" -center com -all
# Avoid all connection issues from the PBC
pbc join connected -all