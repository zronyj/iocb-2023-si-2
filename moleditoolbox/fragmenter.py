import re                              # To find substrings using RegEx
import os                              # A library to manage the file system
import shutil                          # Library to manage more of the file system
import numpy as np                     # To do basic scientific computing
import argparse as ap                  # To take command line arguments nicely
from matplotlib import pyplot as plt   # To plot data
from moledito import Atom, Molecule, Cluster # To handle the atomic and molecular data

gaussian_light = """%chk=XX-{name}.chk
%mem=16000MB
%nproc=4
#B3PW91/6-31G* freq=noraman
SCRF(CPCM,Solvent=Water) nosymm
scf=(xqc,conver=8)
IOp(2/11=1) EmpiricalDispersion=GD3BJ

QGRAD opt {name}

{charge} {multiplicity}
"""

gaussian_heavy = """%chk=XX-{name}.chk
%mem=16000MB
%nproc=4
#B3PW91/6-311++G** freq=noraman
SCRF(CPCM,Solvent=Water) nosymm
scf=(xqc,conver=8)
IOp(2/11=1) EmpiricalDispersion=GD3BJ

QGRAD opt {name}

{charge} {multiplicity}
"""

qgrad_input = """wmin
-100
wmax
+100
glimit
0.0001
qmax
0.4
end"""

def distance_r3(atom1, atom2):
    r"""Compute the cartesian distance between atoms

    This function is intended to create the initial
    coordinates file.

    Parameters
    ----------
    atom1 : list of [str, float, float, float]
        The symbol of the atom and XYZ coordinates
    atom2 : list of [str, float, float, float]
        The symbol of the atom and XYZ coordinates

    Returns
    -------
    float
        Cartesian distance between atoms 1 and 2
    """

    d = [(atom1[i] - atom2[i])**2 for i in range(1,4)]
    return sum(d)**0.5


def read_pdb(file_name):
    """ Open a PDB file and read the data

    This function opens a PDB file and reads each atom,
    takes the coordinates and identity, and builds a
    Molecule object with each molecule, adding them all
    to a cluster dictionary """

    # Read the data from the PDB
    with open(file_name, 'r') as pdb:
        data = pdb.readlines()

    # Filter out anything that's not an atom
    data = [l for l in data if "ATOM" in l]

    # Create a list of molecules
    cluster = {}

    # Iterate over atoms
    for l in data:

        # Split the data at X
        pre_coords, post_coords = l.split("X")

        # Split the data previous to X
        temp1 = pre_coords.split()
        pdb_ida = int(temp1[1])
        pdb_name = temp1[3]

        # Split the data after X
        temp2 = post_coords.split()
        pdb_idm = int(temp2[0])
        pdb_sym = temp2[-1]
        pdb_x, pdb_y, pdb_z = [float(q) for q in temp2[1:4]]

        # Create a molecule name
        mol_name = f"{pdb_name}_{pdb_idm:04}"

        # Create the atom object
        temp_atom = Atom(pdb_sym, pdb_x, pdb_y, pdb_z)

        # Create a Molecule object
        if not (mol_name in cluster.keys()):
            cluster[mol_name] = Molecule(mol_name)

        # Add the atom
        cluster[mol_name].add_atoms(temp_atom)

    return cluster

def read_xyz_cluster(file_name):
    """ Open an XYZ file and read the molecular cluster data

    This function opens a XYZ file and reads each atom,
    takes the coordinates and identity, and builds a
    Molecule object with each molecule, adding them all
    to a cluster dictionary """

    # Read the data from the XYZ
    with open(file_name, 'r') as f:
        data = f.readlines()

    # Extract the identifiers to build the individual molecules
    identifiers = data[1].split("/")
    # Clean the identifiers
    identifiers = [i for i in identifiers if len(i) > 1]
    # Extract the number of atoms from each molecule
    atms_per_mol = re.findall(r'\[\d+\]', data[1])
    # Turn those to integers
    atms_per_mol = [int(numat[1:-1]) for numat in atms_per_mol]
    # Sanity check
    if sum(atms_per_mol) != int(data[0]):
        raise ValueError(("The number of atoms in the XYZ file doesn't "
                        "match the reported atoms!"))
    # Initialize the cluster
    cluster = {}

    # Initialize a pointer
    ctrl = 2

    # Iterate over all molecules
    for idm, mol in enumerate(identifiers):

        # Extract the molecule name
        temp_mol = Molecule(mol[:8])

        # Iterate over all atoms
        for ida in range(atms_per_mol[idm]):

            # Get the position of the atom using the pointer
            temp = data[ida + ctrl].split()

            # Add an instance of Atom to the molecule
            temp_mol.add_atoms(Atom(temp[0],
                                    float(temp[1]),
                                    float(temp[2]),
                                    float(temp[3])
                                    ))
        # Add the molecule to the cluster
        cluster[mol[:8]] = temp_mol

    return cluster

def get_distances(mol, clust, threshold=False):
    """ Compare the distances between molecules

    This function checks the distances of each atom of
    each molecule to each atom of the reference molecule.
    It stores the shortest for each case and sorts them. """

    # Get all molecule names except the reference molecule
    alt_mols = [m for m in clust.keys() if m != mol]

    # Extract coordinates from the reference molecule
    mol_atms = clust[mol].get_coords()

    # Initialize intermolecular the distances array
    inter_mol_dist = []

    # Iterate over all molecules except for the reference molecule
    for molecule in alt_mols:

        # Extract coordinates from the current molecule
        temp_atms = clust[molecule].get_coords()

        # Initialize interatomic distances array
        inter_atom_dist = []

        # Iterate over atoms of reference molecule
        for a1 in mol_atms:

            # Iterate over atoms of current molecule
            for a2 in temp_atms:

                # Compute distance in R3
                d = distance_r3(a1, a2)

                # Add distance with other data to interatomic array
                inter_atom_dist.append([mol, a1[0], molecule, a2[0], d])

        # Sort interatomic array
        inter_atom_dist.sort(key=lambda s: s[4])

        # Add closest distance to intermolecular array
        inter_mol_dist.append(inter_atom_dist[0])

    # Sort intermolecular array
    inter_mol_dist.sort(key=lambda s: s[4])

    # If there is a distance threshold, filter the distances array
    if threshold:
        inter_mol_dist = [d for d in inter_mol_dist if d[4] <= threshold]

    return inter_mol_dist

def plot_distances(inter_mol):

    # Extract the distances from the list
    axes = [[i, e[4]] for i, e in enumerate(inter_mol)]

    # Unpack the axes
    X, Y = zip(*axes)

    # Plot the whole thing
    plt.plot(X, Y, "b-")
    plt.show()

def extract_mols(mols, clust, nam, indiv=False):

    # Create a template for the XYZ coordinates
    template = " {s} {x:16.8f} {y:16.8f} {z:16.8f}\n"

    if indiv:
        # Iterate over all the provided molecules
        for mol in mols:

            mol_coord = clust[mol].get_coords() # Extract coordinates from molecule
            atoms = len(mol_coord)              # Get number of atoms

            # Begin with the XYZ file
            content = f"{atoms}\n{mol} - Extracted from larger cluster\n"

            # Add all atoms to the content of the XYZ file
            for a in mol_coord:
                content += template.format(s=a[0], x=a[1], y=a[2], z=a[3])

            # Save the XYZ file
            with open(f"{mol}_{nam}.xyz", "w") as m:
                m.write(content)
    else:
        # Initialize the atom number
        atoms = 0

        # Initialize the PDB file content
        pdb_cont = "CRYST1    0.000    0.000    0.000  90.00  90.00  90.00 P 1           1\n"

        pdb_template = ("ATOM {num:>6} {s:>2}   {nam} X{molnum:>4}     "
                        "{x:7.3f} {y:7.3f} {z:7.3f}  1.00  0.00          {s:>2}\n")

        # Initialize the XYZ file content
        content = ""

        # Initialize XYZ header
        head = ""

        # Iterate over all the provided molecules
        for idm, mol in enumerate(mols):

            # Extract coordinates from molecule
            mol_coord = clust[mol].get_coords()

            # Add all atoms to the content of the XYZ file
            for ida, a in enumerate(mol_coord):
                content += template.format(s=a[0], x=a[1], y=a[2], z=a[3])
                pdb_cont += pdb_template.format(
                                num=1 + ida + atoms,
                                s=a[0],
                                nam=mol[:3],
                                molnum=int(mol[4:]),
                                x=a[1], y=a[2], z=a[3])

            # Get number of atoms
            temp_atoms = len(mol_coord)
            head += f"{mol}[{temp_atoms}]/"
            atoms += temp_atoms

        # Add header and coordinates toghether
        content = f"{atoms}\n" + head + "\n" + content

        # Finishing the PDB file
        pdb_cont += "END\n"

        # Save the XYZ file
        with open(f"cluster_{nam}.xyz", "w") as m:
            m.write(content)

        # Save the PDB file
        with open(f"cluster_{nam}.pdb", "w") as p:
            p.write(pdb_cont)

def isolate_mols(mols, clust, nam):

    # Get the number of atoms of each molecule
    num_atoms = [mol.get_atoms() for mol in clust.values()]

    # Get the total number of atoms
    all_atoms = sum(num_atoms)

    # Build matrix 1
    matrix1 = list(range(1, all_atoms + 1))

    # Initialize an array with the name of the molecule for each atom
    mol_names_atoms = []

    # Iterate over all molecule names
    for idm, mol in enumerate(clust.keys()):
        # Add the name of the molecule times the number of the atoms
        mol_names_atoms += [mol] * num_atoms[idm]

    # Initialize a pointer
    pointer = 1

    # Initialize matrix 2
    matrix2 = []

    # Iterate over all molecular names for each atom
    for moln in mol_names_atoms:
        # If the name is in the selected atoms ...
        if moln in mols:
            # ... append the pointer!
            matrix2.append(pointer)
            # ... (and increment it)
            pointer += 1
        # Otherwise ...
        else:
            # ... just add a 0
            matrix2.append(0)

    # Initialize the text for the file
    transfer_matrix = ""

    for id1, m1 in enumerate(matrix1):
        transfer_matrix += f"{m1:>3}"
        if id1 != 0 and (id1+1) % 20 == 0:
            transfer_matrix += "\n"
    
    transfer_matrix += "\n"
    for id2, m2 in enumerate(matrix2):
        transfer_matrix += f"{m2:>3}"
        if id2 != 0 and (id2+1) % 20 == 0:
            transfer_matrix += "\n"

    cont = extract_mols(mols, clust, nam)

    with open(f"tranmat_{nam}.dat", "w") as mat:
        mat.write(transfer_matrix)

def get_cluster_coords(clu):
    coords = []

    for val in clu.values():
        coords += val.get_coords()

    return coords

if __name__ == "__main__":

    parser = ap.ArgumentParser(prog="Fragment extractor",
    description="""
Just a small script to find the solvation sphere around
a molecule at some threshold, and extract the fragments
from it. This will be done for all PDB files in current
directory.""",
    epilog=f"(C) 2023 Rony J. Letona [zronyj@gmail.com]")

    # Adding all the command line options to the program
    parser.add_argument('-t', '--threshold', type=float,
        help=("Specify which threshold to use around the molecule to include "
                "solvent molecules as a solvation sphere."),
        required=True)
    parser.add_argument('-n', '--name', type=str,
        help=("Specify the name of the molecule of interest."
                "It should be the name in the PDB file!"),
        required=True)

    # Parsing all the command line arguments
    args = parser.parse_args()

    # Where am I?
    here = os.getcwd()

    # What is here?
    files = os.listdir(here)
    # Filter files
    files = [f for f in files if f[-3:] == 'pdb']

    for f in files:

        clust = read_pdb(f)
        dist = get_distances(args.name, clust, args.threshold)
        solv_sphere = [args.name] + [d[2] for d in dist]

        # Get name of the snapshot
        snapshot = f[:-4]

        # Create temporary folder path
        temp_folder = os.path.join(here, snapshot)

        # Create the folder and move into it
        if os.path.exists(temp_folder):
            shutil.rmtree(temp_folder)
        os.mkdir(temp_folder)
        os.chdir(temp_folder)

        extract_mols(solv_sphere, clust, snapshot)

        solvated = read_xyz_cluster(f"cluster_{snapshot}.xyz")

        new_input = gaussian_light.format(
                                name=snapshot,
                                charge=0,
                                multiplicity=1)
        new_input += "".join(["{} {:8.4f} {:8.4f} {:8.4f}\n".format(*c)\
                         for c in get_cluster_coords(solvated)])
        new_input += "\n532nm\n\n\n"

        with open(f"roa_{snapshot}.com", "w") as gau:
            gau.write(new_input)

        solvent = list(solvated.keys())[1:]

        for ids, sol in enumerate(solvent):

            code = f"0-{ids + 1}"

            # Create temporary folder path for a fragment
            frag_folder = os.path.join(temp_folder, code)

            # Create the folder and move into it
            if os.path.exists(frag_folder):
                shutil.rmtree(frag_folder)
            os.mkdir(frag_folder)
            os.chdir(frag_folder)

            # Create the XYZ and PDB files of the individual fragment
            isolate_mols([args.name, sol], solvated, code)

            frag = Molecule(f'frag_{code}')
            frag.from_xyz(f"cluster_{code}.xyz")

            re_input = gaussian_heavy.format(
                                name=code,
                                charge=0,
                                multiplicity=1)
            re_input += "".join(["{} {:8.4f} {:8.4f} {:8.4f}\n".format(*c)\
                             for c in frag.get_coords()])
            re_input += "\n532nm\n\n\n"

            with open(f"roa_{code}.com", "w") as gau:
                gau.write(re_input)

            # Go back to the snapshot folder
            os.chdir(temp_folder)
        