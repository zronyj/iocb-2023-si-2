import os

template = """%chk={nam}.chk
%mem=6GB
%nproc=8
#b3lyp/6-311++G** scrf=(cpcm,Solvent=Water) nosymm freq=roa
scf=(xqc,conver=8)
iop(2/11=1)
geom=checkpoint
guess=checkpoint

ROA {nam}

0 1

532nm
"""

here = os.getcwd()

dirs = os.listdir()
dirs = [f for f in dirs if os.path.isdir(os.path.join(here,f))]
dirs = [f for f in dirs if "cluster" in f]

for d in dirs:
    os.chdir(os.path.join(here,d))
    with open("FREE.INP", "w") as f:
        f.write(template.format(nam=d))
    os.chdir(here)
