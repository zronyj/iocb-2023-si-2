#!/usr/bin/env python3
# -------------------------------------------------------
#                    Import libraries
# -------------------------------------------------------
import os                          # To handle file system operations
import argparse  as ap             # To accept command line arguments
from multiprocessing import Pool   # To parallelize the process
from moledito import Cluster       # To handle the atomic and molecular data

# -------------------------------------------------------
#                    Define functions
# -------------------------------------------------------

# First, read the cluster as a PDB file
def load_cluster(file_name):
    """Load the system and place its corner at the origin
    
    Load a PDB file, create a Cluster object and move all
    atoms so that they have positive coordinates, resulting
    in the corner of the system being at the origin.
    
    Parameters
    ----------
    file_name : str
        The name of the PDB file with the extension.

    Returns
    -------
    c : Cluster
        A Cluster object containing the coordinates of the
        system and other methods.
    """

    # Remove the extension to have the name of the cluster
    clust_name = file_name[:-4]

    # Create a Cluster object
    c = Cluster('a')
    # Read the coordinates
    c.read_pdb(file_name)
    # Set the origin at the cluster's corner
    c.fix_box()
    # Save the cluster with new coordinates
    c.save_as_pdb(f"{clust_name}_fixed")

    # Return the cluster with new coordinates
    return c

# Then, compute the dimensions of each sub-cluster
def box_dimensions(clust, nbox, noverlap):
    """Compute the coordinates of each box (sub-cluster)
    
    Calculate the number of subclusters and their coordinates
    for each axis of the big cluster.

    Parameters
    ----------
    clust : Cluster
        A Cluster object of the big cluster. It contains all the
        coordinates from the system.
    nbox : int
        The number of boxes per side of the cluster (without overlaps!)
    noverlap : int
        The number of overlapping boxes inside each box.
    
    Returns
    -------
    box_coords : dict
        The lower and upper limit for each of the 3 coordinates
        for each box. Each box is encoded with a set of 3 integers
        representing its position in X, Y and Z.
    
    Note
    ----
        Example of how the boxes work.
        3 boxes:                __0__ __1__ __2__
                               |     |     |     |

        3 boxes, 1 overlap:     __0__ __2__ __4__
                                   ..1.. ..3.. ..5..
                               |  :  |  :  |  :  |  :

        4 boxes:                __0__ __1__ __2__ __3__
                               |     |     |     |     |
        
        4 boxes, 1 overlap:     __0__ __2__ __4__ __6__
                                   ..1.. ..3.. ..5.. ..7..
                               |  :  |  :  |  :  |  :  |  :

        4 boxes, 2 overlaps:    __0__ __3__ __6__ __9__
                                  ..1.. ..4.. ..7.. .10..
                                    ,,2,, ,,5,, ,,8,, ,11,, 
                               | : ; | : ; | : ; | : ; | : ;
    """

    # Get the dimensions of the big cluster
    lims = clust.get_limits()

    # Divide the length of each axis by the number of base boxes
    box_x_side = lims['x'][2] / nbox
    box_y_side = lims['y'][2] / nbox
    box_z_side = lims['z'][2] / nbox

    # Establish the number of boxes within each linear box
    pieces_per_box = noverlap + 1

    # Find the distance of the overlays based on the linear box
    ovrl_x = box_x_side / pieces_per_box
    ovrl_y = box_y_side / pieces_per_box
    ovrl_z = box_z_side / pieces_per_box

    # Compute the number of boxes (including overlays) per side of the big cluster
    num_box_side = pieces_per_box * nbox

    # Provide the user with some info
    print(f"Box size: {box_x_side} x {box_y_side} x {box_z_side}")
    print(f"There will be {num_box_side} boxes per side counting overlaps")
    print(f"And the final number of boxes will be {num_box_side**3}")

    # Get ready to store the coordinates of each box (sub-cluster)
    box_coords = {}

    for k in range(num_box_side):
        for j in range(num_box_side):
            for i in range(num_box_side):
                box_coords[f"{i}-{j}-{k}"] = {
                            'x':tuple([i * ovrl_x, i * ovrl_x + box_x_side]),
                            'y':tuple([j * ovrl_y, j * ovrl_y + box_y_side]),
                            'z':tuple([k * ovrl_z, k * ovrl_z + box_z_side])
                                            }
    return box_coords

# Create a map of the sub-cluster
def atom_submap(clust, mol_names1, mol_names2, map_name):
    """Create the atom map of each sub-cluster

    Make a list of all the atoms of the big cluster and, in
    a new list, put the number of the atom in the sub-cluster,
    in the position corresponding to the same atom in the first
    list. Finally, save this map as a .dat file.

    Parameters
    ----------
    clust : Cluster
        A Cluster object of the big cluster. It contains all the
        coordinates from the system.
    mol_names1 : list of str
        The names of all the molecules in the big cluster.
    mol_names2 : list of str
        The names of all the molecules in the sub-cluster.
    map_name : str
        The name of the file with the map of the atoms.
    
    Example
    -------
        Big cluster:                  Sub-cluster:
        O  6.006  1.498  2.037        O  3.075  2.453  1.761
        H  6.273  2.022  2.876        H  3.899  2.973  1.572
        H  6.276  1.965  1.223        H  2.759  2.288  0.809
        O  3.075  2.453  1.761
        H  3.899  2.973  1.572
        H  2.759  2.288  0.809
        O  0.680  6.794  2.074
        H  0.000  6.435  1.478
        H  1.056  7.626  1.692

        Map:
          1  2  3  4  5  6  7  8  9
          0  0  0  1  2  3  0  0  0 
    """

    matrix1 = range(1, clust._Cluster__natoms + 1)
    matrix2 = [0] * len(matrix1)

    # Initialize a pointer
    pointer = 1

    # Iterate over all molecules in the subcluster
    for mol2 in mol_names2:
        # Iterate over all molecules in the cluster
        for mol1 in mol_names1:
            # If both molecules are the same
            if mol1 == mol2:
                # Find the number of atoms in the list before this molecule
                position = sum([clust.molecules[m].get_atoms() for m in mol_names1[:mol_names1.index(mol1)]])

                # And fill the atoms in the matrix accordingly
                for i in range(clust.molecules[mol1].get_atoms()):
                    matrix2[position + i] = pointer
                    pointer += 1

    # Initialize the text for the file
    transfer_matrix = ""

    # Arrange the first list as a matrix with 3 spaces for each atom number
    # and a total length of 20 atoms.
    for id1, m1 in enumerate(matrix1):
        # Add the number of the atom, give it 3 spaces and
        # align it to the right
        transfer_matrix += f"{m1:>3}"
        # If this is the 20th atom in this line, add a new-line character
        if id1 != 0 and (id1+1) % 20 == 0:
            transfer_matrix += "\n"
    
    # Add a new-line character to begin with the second list
    transfer_matrix += "\n"

    # Arrange the second list as a matrix with 3 spaces for each atom number
    # and a total length of 20 atoms.
    for id2, m2 in enumerate(matrix2):
        # Add the number of the atom, give it 3 spaces and
        # align it to the right
        transfer_matrix += f"{m2:>3}"
        # If this is the 20th atom in this line, add a new-line character
        if id2 != 0 and (id2+1) % 20 == 0:
            transfer_matrix += "\n"

    # Save the two matrices as the map in a .dat file
    with open(f"map_{map_name}.dat", "w") as f:
        f.write(transfer_matrix)

# Wrap everything up
def put_in_boxes(clust, idd, dim, interesting):
    """Create the sub-cluster and its map
    
    Given the name and the dimensions of the sub-cluster
    extract that cluster of molecules from the big cluster,
    save it, and create the atom map for it.

    Parameters
    ----------
    clust : Cluster
        A Cluster object of the big cluster. It contains all the
        coordinates from the system.
    idd : str
        The name of the sub-cluster
    dim : dict of tuple of float
        The lower and upper limits of each dimension of the sub-cluster.
    interesting : str
        The code for the molecule of interest inside the big cluster
    """

    # If the molecule of interest is inside the sub-cluster ...
    if clust.is_in_box(interesting, dim):
        # ... add a flag to the name of the sub-cluster
        flag = "_i"
        # ... otherwise, don't add a flag
    else:
        flag = ""
    
    # Create the sub-cluster given the specified dimensions
    c = clust.sub_cluster(dim)

    # Save the sub-cluster as an XYZ file
    c.save_as_xyz(f"cluster_{idd}{flag}")

    # Create the atom map for the sub-cluster relative to the
    # big cluster.
    atom_submap(
            clust,
            list(clust.molecules.keys()),
            list(c.molecules.keys()),
            idd
                )

# -------------------------------------------------------
#                      Main program
# -------------------------------------------------------

if __name__ == "__main__":

    parser = ap.ArgumentParser(prog="Cluster Dicer",
        description="""
Small script split a big solvated system into boxes (dice).
Only the number of dice per side and the overlaps are required.

Example of boxes and overlaps.
    3 boxes:                 __0__ __1__ __2__
                            |     |     |     |

    3 boxes, 1 overlap:      __0__ __2__ __4__
                                ..1.. ..3.. ..5..
                            |  :  |  :  |  :  |  :

    4 boxes:                 __0__ __1__ __2__ __3__
                            |     |     |     |     |
    
    4 boxes, 1 overlap:      __0__ __2__ __4__ __6__
                                ..1.. ..3.. ..5.. ..7..
                            |  :  |  :  |  :  |  :  |  :

    4 boxes, 2 overlaps:     __0__ __3__ __6__ __9__
                               ..1.. ..4.. ..7.. .10..
                                 ,,2,, ,,5,, ,,8,, ,11,, 
                            | : ; | : ; | : ; | : ; | : ;
    """,
    epilog=f"(C) 2023 Rony J. Letona [zronyj@gmail.com]",
    formatter_class=ap.RawTextHelpFormatter)

    # Adding all the command line options to the program
    parser.add_argument('--pdb', type=str,
        help="Specify the name of the PDB file with the coordinates.",
        required=True)
    parser.add_argument('-b', '--boxes', type=int,
        help="Specify the number of boxes per side.",
        required=True)
    parser.add_argument('-o', '--overlaps', type=int,
        help="Specify the number of overlaps in each small box.",
        required=True)

    # Parsing all the command line arguments
    args = parser.parse_args()
    
    # Translate the arguments into developer-friendly variables
    boxes_per_side = args.boxes
    overlapping_box = args.overlaps
    pdb = args.pdb

    # Get the current working directory
    here = os.getcwd()

    # Load the PDB file, re-locate it in space, and save the new coordinates
    everything = load_cluster(pdb)

    # Compute the lower and upper limits of each box
    dims = box_dimensions(everything, boxes_per_side, overlapping_box)

    # Prepare the arguments to create all sub-clusters and maps
    list_of_arguments = [[everything, idd, dim, "TAR_0001"] for idd, dim in dims.items()]

    # Get the name of the cluster
    code = pdb[:-4]

    # Prepare the path to create a new directory to save all the sub-clusters information
    new_path = os.path.join(here, code)
    # Create the directory and move into it
    os.mkdir(new_path)
    os.chdir(new_path)

    # Start a pool of all available processors
    with Pool() as p:
        # Send all the arguments for parallel processing:
        # .. Create sub-cluster
        # .. Create map
        p.starmap(put_in_boxes, list_of_arguments)