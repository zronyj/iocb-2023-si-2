-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -607.3127044616
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 38.9999503155 
   Number of Beta  Electrons                 38.9999503155 
   Total number of  Electrons                77.9999006310 
   Exchange energy                          -57.5987678044 
   Correlation energy                        -3.2653772919 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -60.8641450963 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -607.3127044616 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 16
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.9719     6.0000     0.0281     4.1247     4.1247    -0.0000
  1   0     6.1400     6.0000    -0.1400     3.8600     3.8600    -0.0000
  2   0     5.9511     6.0000     0.0489     4.0337     4.0337    -0.0000
  3   0     0.7310     1.0000     0.2690     0.8289     0.8289     0.0000
  4   0     0.7151     1.0000     0.2849     0.9831     0.9831    -0.0000
  5   0     8.2845     8.0000    -0.2845     1.9060     1.9060    -0.0000
  6   0     8.3097     8.0000    -0.3097     2.0104     2.0104    -0.0000
  7   0     8.1161     8.0000    -0.1161     2.0620     2.0620     0.0000
  8   0     6.1884     6.0000    -0.1884     4.1568     4.1568    -0.0000
  9   0     8.2931     8.0000    -0.2931     1.8709     1.8709    -0.0000
 10   0     0.7239     1.0000     0.2761     0.8206     0.8206    -0.0000
 11   0     8.2748     8.0000    -0.2748     1.9750     1.9750    -0.0000
 12   0     8.2090     8.0000    -0.2090     1.9530     1.9530    -0.0000
 13   0     0.6528     1.0000     0.3472     0.9574     0.9574    -0.0000
 14   0     0.7032     1.0000     0.2968     0.9526     0.9526    -0.0000
 15   0     0.7354     1.0000     0.2646     1.0036     1.0036     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                0.988280
                0             6               6            8                1.890600
                0             6               7            8                1.362401
                0             6               9            8               -0.205527
                1             6               5            8               -0.242609
                1             6               8            6                0.778004
                1             6               9            8                1.368889
                1             6              10            1                0.906826
                2             6               8            6                0.983154
                2             6              11            8                1.910269
                2             6              12            8                1.294593
                3             1               8            6                0.926913
                4             1               7            8                0.830758
                5             8               8            6                1.441705
                5             8              15            1                0.824643
                6             8              13            1                0.147898
                8             6               9            8               -0.124287
                9             8              14            1                0.832168
               12             8              13            1                0.744872
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -607.3127046462
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 38.9999503698 
   Number of Beta  Electrons                 38.9999503698 
   Total number of  Electrons                77.9999007397 
   Exchange energy                          -57.5987684911 
   Correlation energy                        -3.2653782374 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -60.8641467284 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -607.3127046462 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 16
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.9719     6.0000     0.0281     4.1249     4.1249    -0.0000
  1   0     6.1399     6.0000    -0.1399     3.8602     3.8602     0.0000
  2   0     5.9510     6.0000     0.0490     4.0336     4.0336     0.0000
  3   0     0.7309     1.0000     0.2691     0.8288     0.8288     0.0000
  4   0     0.7151     1.0000     0.2849     0.9831     0.9831     0.0000
  5   0     8.2845     8.0000    -0.2845     1.9060     1.9060    -0.0000
  6   0     8.3097     8.0000    -0.3097     2.0103     2.0103    -0.0000
  7   0     8.1160     8.0000    -0.1160     2.0620     2.0620     0.0000
  8   0     6.1886     6.0000    -0.1886     4.1566     4.1566     0.0000
  9   0     8.2931     8.0000    -0.2931     1.8709     1.8709    -0.0000
 10   0     0.7240     1.0000     0.2760     0.8207     0.8207     0.0000
 11   0     8.2749     8.0000    -0.2749     1.9750     1.9750     0.0000
 12   0     8.2090     8.0000    -0.2090     1.9531     1.9531     0.0000
 13   0     0.6528     1.0000     0.3472     0.9573     0.9573     0.0000
 14   0     0.7032     1.0000     0.2968     0.9525     0.9525    -0.0000
 15   0     0.7354     1.0000     0.2646     1.0036     1.0036     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                0.988297
                0             6               6            8                1.890605
                0             6               7            8                1.362373
                0             6               9            8               -0.205486
                1             6               5            8               -0.242654
                1             6               8            6                0.777990
                1             6               9            8                1.368910
                1             6              10            1                0.906866
                2             6               8            6                0.983190
                2             6              11            8                1.910239
                2             6              12            8                1.294648
                3             1               8            6                0.926875
                4             1               7            8                0.830762
                5             8               8            6                1.441727
                5             8              15            1                0.824629
                6             8              13            1                0.147815
                8             6               9            8               -0.124312
                9             8              14            1                0.832171
               12             8              13            1                0.744881
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : tartaric.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.3516709472
        Electronic Contribution:
                  0    
      0       1.151379
      1      -0.191897
      2       1.932397
        Nuclear Contribution:
                  0    
      0      -2.300183
      1       0.429491
      2      -2.534529
        Total Dipole moment:
                  0    
      0      -1.148804
      1       0.237594
      2      -0.602132
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.576801767225   -0.045124351852    1.380236077085
               1 C     -0.122986192361    0.326086705714   -0.031859326989
               2 C      2.299606234387   -0.564495978687   -0.240835968214
               3 H      0.495065450741   -1.757298511174   -0.267892284274
               4 H     -2.316629740926    0.216670209124    0.752255384999
               5 O      0.696876636098   -0.725034619821   -2.027369391039
               6 O      0.214797242895   -0.366407888209    2.242944983064
               7 O     -1.880783609995   -0.002431002572    1.598611697666
               8 C      0.816783922381   -0.770395821174   -0.624180005614
               9 O     -1.276619273838    0.488748484610   -0.834028794154
              10 H      0.420948662295    1.278590110277    0.038791975739
              11 O      3.118852914000   -0.453805134012   -1.122261415728
              12 O      2.611542300217   -0.525260457192    1.042968493869
              13 H      1.804289667238   -0.540422400834    1.617549673435
              14 H     -1.025959935884    0.273578675552   -1.746832998286
              15 H      1.597907489976   -0.575878019750   -2.369508101560
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     2 
    Coordinates:
               0 C     -0.576730386760   -0.044772142246    1.380291579054
               1 C     -0.122956035957    0.326324397534   -0.031855731435
               2 C      2.299560743905   -0.564293681282   -0.240794631135
               3 H      0.495175514148   -1.757166186030   -0.267616718384
               4 H     -2.316600116372    0.216038380846    0.752077238555
               5 O      0.696653296554   -0.725028562442   -2.027267449899
               6 O      0.214939985397   -0.366162155007    2.242887999424
               7 O     -1.880791480925   -0.002997294612    1.598465611417
               8 C      0.816694954243   -0.770285898207   -0.624139485772
               9 O     -1.276551570708    0.489071998388   -0.834067547604
              10 H      0.421060997649    1.278777594940    0.038652027027
              11 O      3.118802880868   -0.453989612868   -1.122290736209
              12 O      2.611528080319   -0.525313739997    1.042983113416
              13 H      1.804253011401   -0.540757448815    1.617585375990
              14 H     -1.025898144038    0.273929288214   -1.746872517999
              15 H      1.597748270276   -0.576254938418   -2.369448126446
