#!/bin/bash

for d in $(ls -d cluster_*/)
do
cd $d

# Create new folder
mkdir roa
# Copy the G16 output to the new folder
cp FREE.OUT roa/
# Get into the folder
cd roa
# Extract data from the G16 output
/share/zronyj/bin/gar9 FREE.OUT
# Transform everything to internal coordinates?
/share/zronyj/bin/new1
# Extract the atomic masses (will be needed later by other programs)
/share/zronyj/bin/new2 0 0 y
# Diagonalizes the Hessian and gets frequencies
/share/zronyj/bin/new4
# Combine F.INP and FILE.TEN into DOG.TAB to compute the IR spectrum
/share/zronyj/bin/new5
# Combine F.INP and FILE.TTT into ROA.TAB to compute the Raman and ROA spectra
/share/zronyj/bin/new6
# Extract the data, filters it, smoothens it, and saves it for the IR spectrum
/share/zronyj/bin/tabprnf DOG.TAB 1 3901 100 4000 l 300 10
mv S.PRN ir.dat
# Extract the data, filters it, smoothens it, and saves it for the Raman spectrum
/share/zronyj/bin/tabprnf ROA.TAB 3 3901 100 4000 l 300 10
mv S.PRN raman.dat
# Extract the data, filters it, smoothens it, and saves it for the ROA spectrum
/share/zronyj/bin/tabprnf ROA.TAB 8 3901 100 4000 l 300 10
mv S.PRN roa.dat

cd ../..
done