#!/bin/bash

mkdir combined

for d in $(ls -d cluster_*/)
do

cluster=${d%?}

cp $cluster/FREE.OUT .
mv FREE.OUT $cluster.out
/share/zronyj/bin/gar9 $cluster.out
mv $cluster.out combined
mv FILE.X combined/$cluster.x
mv FILE.FC combined/$cluster.fc
mv FILE.TEN combined/$cluster.ten
mv FILE.TTT combined/$cluster.ttt
mv POL.TTT combined/$cluster.poltensor
mv A.TTT combined/$cluster.atensor
mv GP.TTT combined/$cluster.gtensor
mv ORT.TTT combined/$cluster.ortensor
rm EP.LST FILE.GR *.f1

done