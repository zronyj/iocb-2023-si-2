#!/bin/bash

for d in $(ls -d cluster_*/)
do
clu=${d%?}
touch $clu/temp.inp
head -1 $clu/G98.INP >> $clu/temp.inp
echo "%mem=6GB" >> $clu/temp.inp
echo "%nproc=8" >> $clu/temp.inp
tail -n +4 $clu/G98.INP >> $clu/temp.inp
mv $clu/temp.inp $clu/G98.INP

touch $clu/temp.inp
head -1 $clu/FREE.INP >> $clu/temp.inp
echo "%mem=6GB" >> $clu/temp.inp
echo "%nproc=8" >> $clu/temp.inp
tail -n +4 $clu/FREE.INP >> $clu/temp.inp
mv $clu/temp.inp $clu/FREE.INP

done