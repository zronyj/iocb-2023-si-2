# Get library for Operating System interaction
import os

# Open and read file ...
f = open("G98.INP", "r")
data = f.readlines() # ... as a list
f.close()

# Delete the original file
os.remove("G98.INP")

# Iterate over all lines
ctrl = 0
for i, line in enumerate(data):
    try:
        # Replace everything after the \r character
        data[i] = data[i][:data[i].index("\r")] + "\n"
        ctrl += 1
    except Exception:
        data[i] = data[i].rstrip() + "\n"
        ctrl += 0

if ctrl > 0:
    print("The file was corrupted!")
    # Write the new and corrected version
    f = open("G98.INP", "w")
    data = f.writelines(data)
    f.close()
else:
    # Write the new and corrected version
    g = open("G98.INP", "w")
    g.writelines(data)
    g.close()
    # Write the Latest Correct Input into a file
    h = open("LCI.INP", "w")
    h.writelines(data)
    h.close()