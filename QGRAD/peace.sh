#!/bin/bash
for d in $(ls -d */)
do
cd $d
/usr/local/g16-A.03/g16 ${d%?}.inp ${d%?}.out
cp ../runopt.sh .
gar $(head -2 FILE.X | tail -1 | xargs) ${d%?}.out
bash runopt.sh
echo $d
cd ..
done
