from matplotlib import pyplot as plt

with open('d.prn', 'r') as f:
	data = f.readlines()

data = [l.split() for l in data]

X, Y = zip(*data)

X = [float(x) for x in X]
Y = [float(x) for x in Y]

plt.plot(X, Y, "r-")
plt.show()
